# This file is a template, and might need editing before it works on your project.
FROM ros:noetic-ros-core-focal

RUN apt-get update \
    && apt-get install -y --no-install-recommends \
    apt-utils \
    build-essential \
    ccache \
    cmake  \
    cmake-curses-gui  \
    curl  \
    doxygen  \
    dpkg-dev  \
    file \
    git \
    mkdocs  \
    ninja-build  \
    libboost-all-dev  \
    python3-pip  \
    python3-setuptools  \
    python3-wheel  \
    software-properties-common  \
    wget  \
    g++-7  \
    g++-8  \
    python3-catkin-tools  \
    python3-numpy  \
    python3-psutil  \
    python3-yaml  \
    python3-deepdiff  \
    ros-noetic-image-geometry  \
    ros-noetic-rosbag  \
    ros-noetic-roscpp  \
    ros-noetic-sensor-msgs  \
    ros-noetic-std-msgs  \
    ros-noetic-tf \
    ros-noetic-tf2  \
    ros-noetic-visualization-msgs \
    && rm -rf /var/lib/apt/lists/*

RUN python3 -m pip install \
    pyparsing==2.4.6  \
    colcon-common-extensions  \
    colcon-mixin

RUN apt-get update \
    && apt-get install -y --no-install-recommends  \
    python3-numpy  \
    python3-psutil  \
    python3-yaml  \
    python3-deepdiff  \
    ros-noetic-image-geometry  \
    ros-noetic-rosbag  \
    ros-noetic-roscpp  \
    ros-noetic-sensor-msgs  \
    ros-noetic-std-msgs  \
    ros-noetic-tf  \
    ros-noetic-tf2  \
    ros-noetic-visualization-msgs \
    && rm -rf /var/lib/apt/lists/*

RUN python3 -m pip install unittest-xml-reporting
